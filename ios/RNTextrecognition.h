
#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif

#import <VisionKit/VisionKit.h>
#import <Vision/Vision.h>

@interface RNTextrecognition : NSObject <RCTBridgeModule>{
    NSMutableArray *results;
}

@end
  
