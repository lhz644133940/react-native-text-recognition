
#import "RNTextrecognition.h"

@implementation RNTextrecognition

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(ocr: (NSURL*)imageUrl findEvents:(RCTResponseSenderBlock)callback){
    if (@available(iOS 13.0, *)){
        dispatch_queue_t queue1 = dispatch_queue_create("videoQueue", DISPATCH_QUEUE_CONCURRENT);
        dispatch_async(queue1, ^{
            UIImage * image;
            NSData * data = [ NSData dataWithContentsOfURL :imageUrl];
            image = [UIImage imageWithData :data];
            self->results = [NSMutableArray array];
            VNImageRequestHandler* myRequestHandler = [[VNImageRequestHandler alloc]initWithCGImage:image.CGImage options:@{}];
            
            VNRecognizeTextRequest* myTextRecognitionRequest = [[VNRecognizeTextRequest alloc]initWithCompletionHandler:^(VNRequest * _Nonnull request, NSError * _Nullable error) {
                NSArray *observations = request.results;
                for (int i = 0; i <= observations.count; i++) {
                    VNRecognizedText * candidate = [observations[i] topCandidates:9].firstObject;
                    NSRange range = NSMakeRange(0, candidate.string.length);
                    VNRectangleObservation *boxObservation =  [candidate boundingBoxForRange:range error:nil];
                    CGRect boxRect =  boxObservation.boundingBox;
                    CGRect normalRect = VNImageRectForNormalizedRect(boxRect, image.size.width, image.size.height);
                    CGFloat y = image.size.height - normalRect.origin.y -normalRect.size.height;
                    NSLog(@"lhz : %f, %f",normalRect.origin.x, normalRect.origin.y);
                    NSDictionary * res = @{
                        @"idx" : [NSNumber numberWithInt:i],
                        @"content" : candidate.string,
                        @"groupID" : @0,
                        @"x":[NSNumber numberWithFloat:normalRect.origin.x],
                        @"y":[NSNumber numberWithFloat:y],
                        @"width":[NSNumber numberWithFloat:normalRect.size.width],
                        @"height":[NSNumber numberWithFloat:normalRect.size.height]
                    };
                    [self->results addObject:res];
                }
            }];
             
            myTextRecognitionRequest.recognitionLevel = VNRequestTrackingLevelAccurate;
            myTextRecognitionRequest.revision = VNRecognizeTextRequestRevision1;
            myTextRecognitionRequest.recognitionLanguages =  @[@"en"];
            myTextRecognitionRequest.usesLanguageCorrection = false;
            
            [myRequestHandler performRequests:@[myTextRecognitionRequest] error:nil];
            callback(@[[NSNull null],self->results]);
            [self->results removeAllObjects];
        });
        
    }else{
        callback(@[@"current system version is lower than the minimum ",self->results]);
        return;
    }
}


@end

